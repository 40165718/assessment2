﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
    /// <summary>
    /// Interaction logic for AddDriver.xaml
    /// </summary>
    public partial class AddDriver : Window
    {
        //Intialized the objects that will be needed for this window/
       Drivers Driver;
       public List<Drivers> NewDrivers = new List<Drivers>();
        //Takes in StandInDrivers from Reference
        public AddDriver(Drivers StandInDrivers)
        {
            InitializeComponent();
            //Makes the Drivers instance equal to what was Brought in.
            Driver = StandInDrivers;
            
        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            //Closes the form
            this.Hide(); 
        }

        public void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            //temp data to fill in blanks if they dont work
            int tempid = 0;
            //Tries to parse the data if not then it cant work.
            if (!int.TryParse(ID.Text, out tempid))
            {
                //if it isnt a number then it shows an error message
                MessageBox.Show("You have not entered a valid ID Number");
            }
            //Adds all the values of the textboxes to their Corresponding variables from the Dirver class
            Driver.name = Name.Text;
            Driver.Reg = REG.Text;
            //Sets the id values to the temp id value
            Driver.id = tempid;
            //Adds this instance to the NewDrivers List
            NewDrivers.Add(Driver);
            //Prints the Values of the list into the List Box.
            DelDriverLst.Items.Add(Driver.name);

        }

        private void DelDriverLst_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Blank Void cause the program doesnt like this not being here
        }

        public void DleDriverBtn_Click(object sender, RoutedEventArgs e)
        {
            //Creates an index that is equal to the Slected Index of the List box
            int index = DelDriverLst.SelectedIndex;
            //Removes from the List the things that are at the index
            NewDrivers.RemoveAt(index);
            //Removes the selected item from the List box as well
            DelDriverLst.Items.RemoveAt(index);
        }
    }
}
