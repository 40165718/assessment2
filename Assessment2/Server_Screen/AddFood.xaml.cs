﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
    /// <summary>
    /// Interaction logic for AddFood.xaml
    /// </summary>
    public partial class AddFood : Window
    {
        // Initializes all the differnent objects we shall need for this window
        private Food food;
        List<Food> NewFood = new List<Food>();
        //takes In StandInFood as a reference
        public AddFood(Food StandInFood)
        {
            //Sets Food to StandInFood
            food = StandInFood;
            InitializeComponent();
        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            // closes the form 
            this.Close(); 
        }

        private void AddFoodBtn_Click(object sender, RoutedEventArgs e)
        {
            //temp data to fill in blanks if they dont work
            int tempprice = 0;
            //Tries to parse the dayta if not then it cant work.
            if (!int.TryParse(Price.Text, out tempprice)) 
            {
                //If it is not a valid message then it displays an error message
                MessageBox.Show("You have not entered a valid Price");
            }
            //Checks Whether or not Vegetarian Check Box is checked or not
            if (VegCheck.IsChecked == true)
            {
                //If it is then it sets the bool value to true
                food.vege = true;
            }
            else
            {
                //If it isnt then it sets the variable to false
                food.vege = false;
            }

            //Sets the Desc value to the value of Desc.text
            food.Desc = Desc.Text;
            //Sets the Price Value to the value of temp Price
            food.Price = tempprice;
            // Adds the current values of food to the NewFood List
            NewFood.Add(food);
            // Then adds the new Food list to the DelFoodLst ListBox
            DelFoodLst.Items.Add(food.Desc);
        }

        private void DelFoodLst_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Empty Void because the Program doesnt like this not being here.
        }

        private void DelFoodBtn_Click(object sender, RoutedEventArgs e)
        {
            //Creates an index then sets it to the value of the slected index
            int index = DelFoodLst.SelectedIndex;
            // Removes the values from the INdex in the NewFood list
            NewFood.RemoveAt(index);
            //Removes the values from the DelFoodLst ListBox that are the index value
            DelFoodLst.Items.RemoveAt(index);
        }



    }
}
