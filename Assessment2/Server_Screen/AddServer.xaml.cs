﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
    /// <summary>
    /// Interaction logic for AddServer.xaml
    /// </summary>
    public partial class AddServer : Window
    {
        //Intializes the items that are needed for this window
        private servers Server;
        public List<servers> NewServers = new List<servers>();
        public AddServer(servers TemporaryServer)
        {
            //Makes the Server Item equal to the Temporary Server Feild
            Server = TemporaryServer;
            InitializeComponent();

        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            // closes the form
            Close();
        }

        private void AddBTN_Click(object sender, RoutedEventArgs e)
        {
            //temp data to fill in blanks if they dont work
            int tempid = 0;
            //Tries to parse the dayta if not then it cant work.
            if (!int.TryParse(id.Text, out tempid)) 
            {
                //If not then it dispalays an error message
                MessageBox.Show("You have not entered a valid ID Number");
            }
            //Adds the values of name.Text to the name vairable in Server
            Server.name = name.Text;
            //Makes the values of id equal the values of tempid
            Server.id = tempid;
            //Adds the current values of Server to the NewServers List
            NewServers.Add(Server);
            //Then it adds the List to the ListBox
            DelServerLst.Items.Add(Server.name);
        }

        private void DelServerbtn_Click(object sender, RoutedEventArgs e)
        {
            //Creates an index  which is the same value as the current value of the selected index
            int index = DelServerLst.SelectedIndex;
            // Removes the Values of NewServer at the index value
            NewServers.RemoveAt(index);
            // Removes the Item from the DelServerLst ListBox at the index
            DelServerLst.Items.RemoveAt(index);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //empty Void because the Program doesnt appreicatie it when i try to delete this
        }
    }
}
