﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15


namespace Server_Screen
{
   public class TakeAway
    {
       //Private variables for testing
        private string Pitems;
        private int Pprice;
        public string Driver;
        public string Address;
        public string Reg;
        public string items
        {
            get { return Pitems; }
            set
            {
                //Checks to see if the string is empty
                if (value != string.Empty)
                {
                    // if it isnt then Pitems becomes values
                    Pitems = value;
                }
                else
                {
                    //otherwise an error message is displayed
                    MessageBox.Show("you have not got any Items On your Bill.");
                }
            }
        }

        public int Price
        {
            get { return Pprice; }
            set
            {
                // checks to see if the string is empty
                if (value.ToString() == string.Empty)
                {
                    // if it is then an error message is displayed
                    MessageBox.Show("Your have Not got a price on your bill");
                }
                //checks to see if the value is less than 0
                else if (value < 0)
                {
                    //if it is then an error message is shown
                    MessageBox.Show("The Bill Price Cannot be less than 0");
                }
                else
                {
                    //otherwise Pprice becomes equal to value.
                    Pprice = value;
                }
            }
        }
    }
}
