﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
    public class Drivers
    {
        //Private variables used for Verification
        private string Pname;
        private int Pid;
        private string PReg;

        public string name
        {
            get { return Pname; }
            set
            {
                //Checks if the value of the string is = null
                if (value != string.Empty)
                {
                    //If it isnt then Pname is to be equal to the Value 
                    Pname = value;
                }
                else
                {
                    // Otherwise it dispays an error message
                    MessageBox.Show("you have not entered a valid name for the driver.");
                }
            }
        }
        public int id
        {
            get { return Pid; }
            set
            {
                //Checks to see if the value is equal to null
                if (value.ToString() == string.Empty)
                {
                    // If it is then it displays an error
                    MessageBox.Show("You need to enter a Number For the ID");
                }
                //Also checks to see if the value if less than 1
                else if (value < 1)
                {
                    //If it is then it displays an error Message
                    MessageBox.Show("You need to enter A value greater than 0 for the ID");
                }
                else
                {
                    // if both of them are false then The Id value is made equal to the value
                    Pid = value;
                }
            }
        }

        public string Reg
        {
            get { return PReg; }
            set
            {
                //Checks to see if the value is equal to nothing
                if (value == string.Empty)
                {
                    //if it is then it displays an error message
                    MessageBox.Show("You Have not entered a value for the registration.");
                }
                // then checks to see if the value is less than 1 or more than 8
                else if (value.Length > 8 || value.Length < 1)
                {
                    //if it is then it dispalys an error message
                    MessageBox.Show("The registration you have enter is either too long or too short;");
                }
                else
                {
                    //otherwise both values are set to values
                    PReg = value;
                }
            }
        }
    }
}
