﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15S

namespace Server_Screen
{
    /// <summary>
    /// Interaction logic for Sever_Page.xaml
    /// </summary>
    public partial class Sever_Page : Window
    {
        //List off all the different Objects and Lists that I need to use in this window.
       public SitIn SitInOrder =  new SitIn();
       public TakeAway TakeAwayOrder = new TakeAway(); 
       public AddDriver driver;
       public AddServer server;
       List<Drivers> CurrentDrivers = new List<Drivers>();
       public List<servers> CurrentServers = new List<servers>();
       List<Food> CurrentFood = new List<Food>();
       public List<SitIn> SitInList= new List<SitIn>();
       public List<TakeAway> TakeAwayList = new List<TakeAway>();
       

       public Sever_Page()
       {
           InitializeComponent();
           //shows the Lable thats says Table Number
           lblTabNum.Visibility = Visibility.Hidden;
           //shows the text box that allows you enter the table number
           TableNumTxt.Visibility = Visibility.Hidden;
           // hides the text box that allows you to enter the address
           AddressTxt.Visibility = Visibility.Hidden;
           // hides the text box that shows the label that says Address
           lblAddress.Visibility = Visibility.Hidden;
       }

        private void Managerbtn_Click(object sender, RoutedEventArgs e)
        {
            // Takes these lists aas refernce to the managager page where they are then taken to their own specific windows.
            Manager_Page manager = new Manager_Page(ref CurrentServers, ref CurrentDrivers, ref CurrentFood);
            // Opens the Manager page
            manager.Show(); 
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Empty Box Because the Program does not work if this isnt here.
        }

        private void GetStaffBtn_Click(object sender, RoutedEventArgs e)
        {
            //Starts Off by clearing the Combo Box so you Dont get overlapping Staff Members.
            Staff.Items.Clear();
            //Checks to see if takeAway check box is ticked and the SitIn one is not.
            if (TakeAwayCheck.IsChecked == true && SitInCheck.IsChecked == false)
            {
                //If it is then it pastes the Drivers into the cmobo Box
                foreach (Drivers Driver1 in CurrentDrivers)
                {
                    Staff.Items.Add(Driver1.name);
                }
                //hides the Lable thats says Table Number
                lblTabNum.Visibility = Visibility.Hidden;
                //hides the text box that allows you enter the table number
                TableNumTxt.Visibility = Visibility.Hidden;
                //shows the text box that allows you to enter the address
                AddressTxt.Visibility = Visibility.Visible;
                //shows the text box that shows the label that says Address
                lblAddress.Visibility = Visibility.Visible;
            }
            //Checks to see if the SitIn Check box is checked and the SitIn one is not.
            else if (SitInCheck.IsChecked == true && TakeAwayCheck.IsChecked == false)
            {
                // If it is then it copies all the Servers Names into the Combo Box
                foreach (servers Server1 in CurrentServers)
                {
                    Staff.Items.Add(Server1.name);

                }
                //shows the Lable thats says Table Number
                lblTabNum.Visibility = Visibility.Visible;
                //shows the text box that allows you enter the table number
                TableNumTxt.Visibility = Visibility.Visible;
                // hides the text box that allows you to enter the address
                AddressTxt.Visibility = Visibility.Hidden;
                // hides the text box that shows the label that says Address
                lblAddress.Visibility = Visibility.Hidden;
            }
            //If both of them or neither of them are checked then an error message appears.
            else
            {
                MessageBox.Show("You need to check whether it is a Sit In or Takeaway Order.");
            }
        }

        private void GetFoodBtn_Click(object sender, RoutedEventArgs e)
        {
            Foodlst.Items.Clear();
            // Prints the Names of the Food into the List box
            foreach (Food Food1 in CurrentFood)
            {
                Foodlst.Items.Add(string.Format("{0} | {1}", Food1.Desc, (Food1.Price)/100));
            }
        }

        private void AddFoodBtn_Click(object sender, RoutedEventArgs e)
        {
            //Checks to see if You are sit in or take away
            if (SitInCheck.IsChecked == true && TakeAwayCheck.IsChecked == false)
            {
                string splits = Foodlst.SelectedItem.ToString();
                string[] currentsplit = splits.Split('|');
                for (int i = 0; i < currentsplit.Length; i++)
                {
                    MessageBox.Show(currentsplit[i].ToString());
                    currentsplit[i].Trim();
                }
                string currentitem = currentsplit[0];
                string currentprice = currentsplit[1];
                int TempPrice;
                if (!int.TryParse(currentprice, out TempPrice))
                {
                    //if it isnt a number then it shows an error message
                    MessageBox.Show("You have not entered a valid ID Number");
                }
                TakeAwayOrder.Price = TempPrice;
                //Sets the currentstaff variable to the currently selected person in the comboBox
                string currentstaff = Staff.SelectedItem.ToString();
                //Then Sets the server is saved as the SitIn classes Server field.
                SitInOrder.Server = currentstaff;
                //Sets the currentitem variable to the currently selected item in the Foodlst List Box
                int temptablenumber;
                if (!int.TryParse(TableNumTxt.Text, out temptablenumber))
                {
                    MessageBox.Show("That is Not a Valid Table Number");
                }
                SitInOrder.TableNumber = temptablenumber;
                //Sets the SitIn classes items field to the value of currentitem
                SitInOrder.items = currentitem;
                //takes the current value of SitIn and addes it to the SitInOrderlist
                SitInList.Add(SitInOrder);
            }
            //Checks to see if you are a take away or a sit in order.
            else if (SitInCheck.IsChecked == false && TakeAwayCheck.IsChecked == true)
            {
                string splits = Foodlst.SelectedItem.ToString();
                string[] currentsplit = splits.Split('|');
                for (int i = 0; i < currentsplit.Length; i++)
                {
                    currentsplit[i].Trim();
                }
                string currentitem = currentsplit[0];
                string currentprice = currentsplit[1];
                //Create an index variable to be used to find price of the item later on 
                int index = Staff.SelectedIndex;
                //Sets the value of current staff to the slected driver in the combo box
                string currentstaff = Staff.SelectedIndex.ToString();
                // Sets the Driver variable of the TakeAwayOrder to the value of current staff
                TakeAwayOrder.Driver = currentstaff;
                int TempPrice;
                if (!int.TryParse(currentprice, out TempPrice))
                {
                    //if it isnt a number then it shows an error message
                    MessageBox.Show("You have not entered a valid ID Number");
                }
                TakeAwayOrder.Price = TempPrice;
                // Creates the variable current item and sets it to the value of the of the slected item in the FoodLst listbox
                //sets the address value to the text box called address.text
                TakeAwayOrder.Address = AddressTxt.Text;
                //Sets the value of the items from the takeAwayorder class to the value of current items.
                TakeAwayOrder.items = currentitem;
                //Adds all the current values of the TakeAwayOrder Class and add them to the TakeAwayList List.
                TakeAwayList.Add(TakeAwayOrder);
            }
            
        }

        private void BillBtn_Click(object sender, RoutedEventArgs e)
        {
            //Creates a Boolean to be used in the BillWindow Form and sets it  to true.
            bool sitin = true;
            //Checks If you are  A sit In diner
            if (SitInCheck.IsChecked == true && TakeAwayCheck.IsChecked == false)
            {
                //If you are then it sets the boolean to true
                sitin = true;
            }
            //Checks If you are a take away diner
            else if (SitInCheck.IsChecked == false && TakeAwayCheck.IsChecked == true)
            {
                //If you are then it sets the boolean to false
                sitin = false;
            }
              //Creates a new instance of billWindow and transfers all the data it needs by references
              BillWindow BillWindow = new BillWindow(ref SitInList,ref TakeAwayList, ref sitin);
            //Shows the BillWindow.
              BillWindow.Show();
        }
    }
}
