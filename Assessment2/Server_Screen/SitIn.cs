﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15
namespace Server_Screen
{
   public class SitIn
    {
        private string Pitems;
        private int Pprice;
        private int PTabNum;
        public int TableNumber
        {
            get { return PTabNum;}
            set
            {
                //Checks to make suure string isnt empty
                if (value.ToString() == String.Empty)
                {
                    MessageBox.Show("You have not entered a value for table Number");
                }
                //Checks to make sure the Table numbver is between 1 and 10
                else if (value < 1 || value > 10)
                {
                    MessageBox.Show("That is not a valid Table Number, It must be between 1 and 10");
                }
                else
                {
                    PTabNum = value;
                }
            }
        }
        public string Server;

        public string items
        {
            get { return Pitems; }
            set
            {
                // checks to see if the value is not equalto zero
                if (value != string.Empty)
                {
                    // if it isnt then the Pitems is set to values
                    Pitems = value;
                }
                else
                {
                    // otherwise an error message is displayed 
                    MessageBox.Show("you have not got any Items On your Bill.");
                }
            }
        }

        public int Price
        {
            get { return Pprice; }
            set
            {
                //checks to see if the string is empty
                if (value.ToString() == string.Empty)
                {
                    // if it is then an error message is displayed
                    MessageBox.Show("Your have Not got a price on your bill");
                }
                // then it checks whether or not the value is less than 0
                else if (value < 0)
                {
                    // if it is then an error message is displayed
                    MessageBox.Show("The Bill Price Cannot be less than 0");
                }
                else
                {
                    // otherwise the value is equal to Pprice
                    Pprice = value;
                }
            }
        }
    }
}
