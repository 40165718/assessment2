﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
    /// <summary>
    /// Interaction logic for BillWindow.xaml
    /// </summary>
    public partial class BillWindow : Window
    {
        List<SitIn> SitInOrderList;
        List<TakeAway> TakeAwayOrderList;
        public BillWindow(ref List<SitIn> TempSitInOrder,ref List<TakeAway> TempTakeAwayOrder,ref bool OrderType)
        {
            InitializeComponent();
            //Hides all the optional Components Until they are needed
            LblServerName.Visibility = Visibility.Hidden;
            TxtServerName.Visibility = Visibility.Hidden;
            LblTableNumber.Visibility = Visibility.Hidden;
            TxtTableNum.Visibility = Visibility.Hidden;
            LblDriverName.Visibility = Visibility.Hidden;
            LblAddress.Visibility = Visibility.Hidden;
            TxtDriverName.Visibility = Visibility.Hidden;
            TxtAddress.Visibility = Visibility.Hidden;
            //Checks if the ordertype is true (OrderType == true means you are sittin in)
            if (OrderType == true)
            {
                //If it is then it makes SitInOrderList = TempSitInOrder
                SitInOrderList = TempSitInOrder;
                //Loops for each time A SitIn Struct is the list
                foreach (SitIn SitIn1 in SitInOrderList)
                {
                    //Then adds the Names of the Dishes and the Names of the Servers to the List Box
                    LstItems.Items.Add(SitIn1.items);
                    //Starts showing all the optional parts for SitIn orders
                    LblServerName.Visibility = Visibility.Visible;
                    TxtServerName.Visibility = Visibility.Visible;
                    //Makes the Value in TxtServerName into The valkue of Server
                    TxtServerName.Text = SitIn1.Server;
                    LstPrice.Items.Add(SitIn1.Price);
                    //Shows the rest of the things required
                    LblTableNumber.Visibility = Visibility.Visible;
                    TxtTableNum.Visibility = Visibility.Visible;
                    //Makes the value of TxtTableNum equa;l to the table number value.
                    TxtTableNum.Text = SitIn1.TableNumber.ToString();
                }
            }
            else
            {
                //It recgnises you are a takeaway order and makes TakeAwayOrder = TempTakeAwayOrder
                TakeAwayOrderList = TempTakeAwayOrder;
                //Loops for every Time A TakeAway Struct appears in the TakeAwayOrderList
                foreach (TakeAway TakeAway1 in TakeAwayOrderList)
                {
                    //Then it adds the appropriate items to the List Box
                    LstItems.Items.Add(TakeAway1.items);
                    LstPrice.Items.Add(TakeAway1.Price);
                    // Starts Showing most of the Needed Items for A TakeAway Order
                    LblDriverName.Visibility = Visibility.Visible;
                    LblAddress.Visibility = Visibility.Visible;
                    TxtDriverName.Visibility = Visibility.Visible;
                    // makes The vale of TxtDriver equal to the value of Driver
                    TxtDriverName.Text = TakeAway1.Driver;
                    // Shows the rest of the Items required to be seen
                    TxtAddress.Visibility = Visibility.Visible;
                    //Makes the Values of TxtAddress equal to the address of the takeAway order.
                    TxtAddress.Text = TakeAway1.Address;
                }
            }

        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            //Closes the Form
            Close();
        }
    }
}
