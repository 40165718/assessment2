﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
   public class Food
    {
       //Private variable for testing Purposes
        private string Pdesc;
        private int Pprice;
        public Boolean vege;
        private string Pveg;

        public string Desc
        {
            get { return Pdesc; }
            set
            {
                //Checlks to see if the Description has got any input of not
                if (value != string.Empty)
                {
                    //if it does then it sets it too the value
                    Pdesc = value;
                }
                else
                {
                    //if it doesnt then an error message is posted
                    MessageBox.Show("You need to Enter A description For the food");
                }
            }
        }
        public int Price
        {
            get { return Pprice; }
            set
            {
                //Checlks to see if the string is empty 
                if (value.ToString() == string.Empty)
                {
                    //if it is then an error message is displyed
                    MessageBox.Show("You have not entered a value to the Price field.");
                }
                    //checks to see if the value is less than 0
                else if (value <= 0)
                {
                    //if it is then an error message is displayed
                    MessageBox.Show ("you need to enter a Price For your menu item.");
                }
                else
                {
                    //otherwise the value is made equal to Pprice
                    Pprice = value;
                }
            }
        }

        public string veg
        {
            get { return Pveg; }
            set
            {
                //checks to see if the string is empty
                if (value != string.Empty)
                {
                    //if it isnt then an  error message is displayed
                    MessageBox.Show("You need to say whether the value is Vegetarian or Not.");
                }
                else
                {
                    //otherwise it is fine.
                    Pveg = value;
                }
            }
        }

    }
}
