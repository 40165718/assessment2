﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15

namespace Server_Screen
{
    /// <summary>
    /// Interaction logic for Manager_Page.xaml
    /// </summary>
    public partial class Manager_Page : Window
    {
        List<servers> tempServers;
        List<Drivers> tempDrivers;
        List<Food> tempFood;
        public Manager_Page(ref List<servers> temp, ref List<Drivers> temporary, ref List<Food> refFood)
        {
            tempServers = temp;
            tempDrivers = temporary;
            tempFood = refFood;
            InitializeComponent();
        }

        //shows the Order Page form
        private void OrderPageBtn_Click(object sender, RoutedEventArgs e)
        {
            //Sever_Page Order = new Sever_Page();
            //Order.Show();
            Close();
        }

        //Closes this form
        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //Shows the Add Server Button
        private void AddServerBtn_Click(object sender, RoutedEventArgs e)
        {
            servers StandInData = new servers();
            AddServer NewServer = new AddServer(StandInData);
            NewServer.ShowDialog();
            tempServers.Add(StandInData);
        }

        //Shows the Add Food Form
        private void AddFoodBtn_Click(object sender, RoutedEventArgs e)
        {
            Food StandInFood = new Food();
            AddFood NewFood = new AddFood(StandInFood);
            NewFood.Show();
            tempFood.Add(StandInFood);

        }

        //Shows the Add Driver Button
        private void AddDriverBtn_Click(object sender, RoutedEventArgs e)
        {
            Drivers StandInDrivers = new Drivers();
            AddDriver NewDriver = new AddDriver(StandInDrivers);
            NewDriver.ShowDialog();
            tempDrivers.Add(StandInDrivers);
        }
    }
}
