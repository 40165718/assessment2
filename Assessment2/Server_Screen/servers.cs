﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//401165718
//SD2
// 11/12/15


namespace Server_Screen
{
   public class servers
    {
       //Private variables for testing
        private string Pname;
        private int Pid;

        public string name
        {
            get { return Pname; }
            set
            {
                //checks to see if the string is empty
                if(value != string.Empty)
                {
                    //if it isnt then the value is set to Pname
                    Pname = value;
                }
                else
                {
                    //otherwise an error message is dispayed
                    MessageBox.Show("you have not entered a valid name for the server");
                }
            }
        }
        public int id
        {
            get { return Pid; }
            set
            {
                //checks to see if the string is empty
                if (value.ToString() == string.Empty)
                {
                    //if it is then an error message is displayed
                    MessageBox.Show("You need to enter a Number For the ID");
                }
                //then checks if the value is less than 1
                else if (value < 1)
                {
                    //If it is then an error message is dispalyed
                    MessageBox.Show("You need to enter A value greater than 0 for the ID");
                }
                else
                {
                    //Otherwise Pid is set to the value
                    Pid = value;
                }
            }
        }

    }
}
